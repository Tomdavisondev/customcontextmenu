﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ScreenID
{
    class WindowManager
    {
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hwnd);

        public string previousWindow;
        public string GetActiveProcessName()
        {
            IntPtr hwnd = GetForegroundWindow();
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            Process p = Process.GetProcessById((int)pid);
            return p.ProcessName;
        }
        public void SelectWindow(string previousWindow)
        {
            var proc = Process.GetProcessesByName(previousWindow).FirstOrDefault();
            Process.GetProcesses();
            if (proc != null && proc.MainWindowHandle != IntPtr.Zero)
                SetForegroundWindow(proc.MainWindowHandle);
        }
    }
}
