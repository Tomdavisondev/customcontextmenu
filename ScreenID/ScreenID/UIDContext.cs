﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapplo.Log;
using Dapplo.Windows.Input.Enums;
using Dapplo.Windows.Input.Keyboard;

namespace ScreenID
{
    public partial class UIDContext : Form
    {
        WindowManager windowManager;

        public UIDContext()
        {
            InitializeComponent();
            windowManager = new WindowManager();

            var openMenuHandler = new KeyCombinationHandler(VirtualKeyCode.LeftMenu, VirtualKeyCode.KeyB)
            {
                IgnoreInjected = false,
                IsPassThrough = false
            };
            KeyboardHook.KeyboardEvents.Where(openMenuHandler).Subscribe(KeyboardHookEventArgs => OpenContextMenuHotkey());
            var closeMenuHandler = new KeyCombinationHandler(VirtualKeyCode.Escape)
            {
                IgnoreInjected = false,
                IsPassThrough = false
            };
            KeyboardHook.KeyboardEvents.Where(closeMenuHandler).Subscribe(KeyboardHookEventArgs => CloseContextMenuHotkey());

            this.WindowState = FormWindowState.Minimized;
        }
        private void LoadFromCSV(string filename)
        {
            try
            {
                using (var reader = new StreamReader(@filename))
                {
                    List<string> tagList = new List<string>();
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(';');
                        //TOOD: Trim away quotes
                        if (values[0] != "")
                        {
                            tagList.Add(values[0]);
                        }
                        UIDTagContextMenu.Items.Add(values[0]);
                    }
                }
                MessageBox.Show("CSV Added to Context Strip");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void CloseContextMenuHotkey()
        {
            UIDTagContextMenu.Hide();
        }
        private void OpenContextMenuHotkey()
        {
            if (windowManager.GetActiveProcessName() != null)
                windowManager.previousWindow = windowManager.GetActiveProcessName();
            UIDTagContextMenu.Show(Cursor.Position);
            windowManager.SelectWindow(Process.GetCurrentProcess().ProcessName);
            FilterTextBox.Focus();
        }
        private void UIDTagContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            UIDTagContextMenu.Hide();
            windowManager.SelectWindow(windowManager.previousWindow);
            SendKeys.Send(e.ClickedItem.Text);
        }

        private void notifyIcon1_MouseDoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }

        private void FilterTextBox_TextChanged(object sender, EventArgs e)
        {
            //Filter
            UIDTagContextMenu.SuspendLayout();
            foreach(ToolStripItem item in UIDTagContextMenu.Items)
            {
                if(!item.Text.ToLower().Contains(FilterTextBox.Text.ToLower()))
                {
                    item.Visible = false;
                }
                else
                {
                    item.Visible = true;
                }
            }
            UIDTagContextMenu.ResumeLayout();
        }

        private void importCsv_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            LoadFromCSV(openFileDialog1.FileName);
        }
    }
}
