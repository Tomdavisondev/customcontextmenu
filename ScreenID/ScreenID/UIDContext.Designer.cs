﻿

namespace ScreenID
{
    partial class UIDContext
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UIDContext));
            this.UIDTagContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.FilterTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.importCsv = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.UIDTagContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // UIDTagContextMenu
            // 
            this.UIDTagContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.UIDTagContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FilterTextBox,
            this.testToolStripMenuItem,
            this.testToolStripMenuItem1,
            this.testToolStripMenuItem2});
            this.UIDTagContextMenu.MaximumSize = new System.Drawing.Size(0, 300);
            this.UIDTagContextMenu.Name = "UIDTagContextMenu";
            this.UIDTagContextMenu.ShowImageMargin = false;
            this.UIDTagContextMenu.Size = new System.Drawing.Size(236, 117);
            this.UIDTagContextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.UIDTagContextMenu_ItemClicked);
            // 
            // FilterTextBox
            // 
            this.FilterTextBox.Name = "FilterTextBox";
            this.FilterTextBox.Size = new System.Drawing.Size(200, 23);
            this.FilterTextBox.TextChanged += new System.EventHandler(this.FilterTextBox_TextChanged);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.testToolStripMenuItem.Text = "[UI-FB-001 Bootloader]";
            // 
            // testToolStripMenuItem1
            // 
            this.testToolStripMenuItem1.Name = "testToolStripMenuItem1";
            this.testToolStripMenuItem1.Size = new System.Drawing.Size(235, 22);
            this.testToolStripMenuItem1.Text = "[UI-FB-002 Kernel Loading]";
            // 
            // testToolStripMenuItem2
            // 
            this.testToolStripMenuItem2.Name = "testToolStripMenuItem2";
            this.testToolStripMenuItem2.Size = new System.Drawing.Size(235, 22);
            this.testToolStripMenuItem2.Text = "[UI-PWR-001 Loading Application]";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Test";
            this.notifyIcon1.BalloonTipTitle = "test";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // importCsv
            // 
            this.importCsv.Location = new System.Drawing.Point(12, 12);
            this.importCsv.Name = "importCsv";
            this.importCsv.Size = new System.Drawing.Size(75, 23);
            this.importCsv.TabIndex = 1;
            this.importCsv.Text = "Import CSV";
            this.importCsv.UseVisualStyleBackColor = true;
            this.importCsv.Click += new System.EventHandler(this.importCsv_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // UIDContext
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 234);
            this.Controls.Add(this.importCsv);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UIDContext";
            this.ShowInTaskbar = false;
            this.Text = "ScreenID Options";
            this.UIDTagContextMenu.ResumeLayout(false);
            this.UIDTagContextMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip UIDTagContextMenu;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem2;
        private System.Windows.Forms.ToolStripTextBox FilterTextBox;
        private System.Windows.Forms.Button importCsv;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

